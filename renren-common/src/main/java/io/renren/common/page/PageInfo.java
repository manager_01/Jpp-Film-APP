package io.renren.common.page;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "分页数据")
public class PageInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private int currentPage;

    private int pageSize;
}
