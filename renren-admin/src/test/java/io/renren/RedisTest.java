/**
 * Copyright (c) 2018 人人开源 All rights reserved.
 * <p>
 * https://www.renren.io
 * <p>
 * 版权所有，侵权必究！
 */

package io.renren;

import cn.hutool.json.JSONUtil;
import io.renren.common.redis.RedisUtils;
import io.renren.modules.sys.entity.SysUserEntity;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class RedisTest {
    @Autowired
    private RedisUtils redisUtils;

    @Test
    public void contextLoads() {
        SysUserEntity user = new SysUserEntity();
        user.setEmail("123456@qq.com");
        redisUtils.set("user", user);

        System.out.println(ToStringBuilder.reflectionToString(redisUtils.get("user")));
    }

    @Test
    public void test01() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", "张三1");
        map.put("age", 23);

        redisUtils.set("test01", JSONUtil.toJsonStr(map));

        String test01 = redisUtils.get("test01").toString();
        log.info("值：{}", test01);
    }

    @Test
    public void test02() {
        redisUtils.hSet("zhangsan", "age", 21);
        redisUtils.hSet("zhangsan", "weight", 80);

        String age = redisUtils.hGet("zhangsan", "age").toString();
        log.info("张三年龄：{}", age);

        Map<String, Object> map = redisUtils.hGetAll("zhangsan");
        log.info("all-{}", map);
    }
}