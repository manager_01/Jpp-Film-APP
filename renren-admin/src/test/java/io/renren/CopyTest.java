package io.renren;

import com.alibaba.fastjson.JSON;
import io.renren.modules.axiostest.dto.Dept;
import io.renren.modules.axiostest.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CopyTest {

    @Test
    public void test01() {
        UserDTO userDTO = new UserDTO();
        userDTO.setAge(10);

        Dept dept = new Dept();
        dept.setDName("开发部");

        userDTO.setDept(dept);

        UserDTO userDTO2 = new UserDTO();

        BeanUtils.copyProperties(userDTO, userDTO2);

        userDTO2.setAge(20);

        Dept deptCopy = userDTO2.getDept();
        deptCopy.setDName("实施部");
        userDTO2.setDept(deptCopy);

        log.info("userDTO:{}", userDTO);
        log.info("userDTO2:{}", userDTO2);
    }

    @Test
    public void testDeepCopy() {
        UserDTO userDTO = new UserDTO();
        userDTO.setAge(10);

        Dept dept = new Dept();
        dept.setDName("开发部");

        userDTO.setDept(dept);

        String userString = JSON.toJSONString(userDTO);
        UserDTO userDTO2 = JSON.parseObject(userString, UserDTO.class);

        Dept deptCopy = userDTO2.getDept();
        deptCopy.setDName("实施部");
        userDTO2.setDept(deptCopy);

        log.info("userDTO:{}", userDTO);
        log.info("userDTO2:{}", userDTO2);
    }
}
