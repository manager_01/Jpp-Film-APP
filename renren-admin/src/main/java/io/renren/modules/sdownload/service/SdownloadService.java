package io.renren.modules.sdownload.service;

import io.renren.common.service.CrudService;
import io.renren.modules.sdownload.dto.SdownloadDTO;
import io.renren.modules.sdownload.entity.SdownloadEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-08
 */
public interface SdownloadService extends CrudService<SdownloadEntity, SdownloadDTO> {

}