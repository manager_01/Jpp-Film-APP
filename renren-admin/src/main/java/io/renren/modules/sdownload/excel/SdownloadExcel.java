package io.renren.modules.sdownload.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-08
 */
@Data
public class SdownloadExcel {
    @Excel(name = "")
    private Integer dlid;
    @Excel(name = "")
    private Integer uid;
    @Excel(name = "")
    private String filename;
    @Excel(name = "")
    private Date time;
    @Excel(name = "")
    private Integer bonus;
    @Excel(name = "")
    private Integer type;
    @Excel(name = "")
    private String descr;
    @Excel(name = "")
    private Integer dstate;
    @Excel(name = "")
    private String url;
    @Excel(name = "")
    private Integer state;
    @Excel(name = "")
    private String reason;
    @Excel(name = "")
    private Integer dnum;
    @Excel(name = "")
    private Integer deltag;

}