package io.renren.modules.sdownload.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-08
 */
@Data
@ApiModel(value = "")
public class SdownloadDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Integer dlid;

	@ApiModelProperty(value = "")
	private Integer uid;

	@ApiModelProperty(value = "")
	private String filename;

	@ApiModelProperty(value = "")
	private Date time;

	@ApiModelProperty(value = "")
	private Integer bonus;

	@ApiModelProperty(value = "")
	private Integer type;

	@ApiModelProperty(value = "")
	private String descr;

	@ApiModelProperty(value = "")
	private Integer dstate;

	@ApiModelProperty(value = "")
	private String url;

	@ApiModelProperty(value = "")
	private Integer state;

	@ApiModelProperty(value = "")
	private String reason;

	@ApiModelProperty(value = "")
	private Integer dnum;

	@ApiModelProperty(value = "")
	private Integer deltag;


}