package io.renren.modules.sdownload.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.sdownload.entity.SdownloadEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-08
 */
@Mapper
public interface SdownloadDao extends BaseDao<SdownloadEntity> {
	
}