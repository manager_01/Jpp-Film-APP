package io.renren.modules.sdownload.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.sdownload.dao.SdownloadDao;
import io.renren.modules.sdownload.dto.SdownloadDTO;
import io.renren.modules.sdownload.entity.SdownloadEntity;
import io.renren.modules.sdownload.service.SdownloadService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-08
 */
@Service
public class SdownloadServiceImpl extends CrudServiceImpl<SdownloadDao, SdownloadEntity, SdownloadDTO> implements SdownloadService {

    @Override
    public QueryWrapper<SdownloadEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SdownloadEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


}