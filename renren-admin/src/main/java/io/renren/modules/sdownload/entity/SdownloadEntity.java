package io.renren.modules.sdownload.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-08
 */
@Data
@TableName("sdownload")
public class SdownloadEntity {

    /**
     * 
     */
	private Integer dlid;
    /**
     * 
     */
	private Integer uid;
    /**
     * 
     */
	private String filename;
    /**
     * 
     */
	private Date time;
    /**
     * 
     */
	private Integer bonus;
    /**
     * 
     */
	private Integer type;
    /**
     * 
     */
	private String descr;
    /**
     * 
     */
	private Integer dstate;
    /**
     * 
     */
	private String url;
    /**
     * 
     */
	private Integer state;
    /**
     * 
     */
	private String reason;
    /**
     * 
     */
	private Integer dnum;
    /**
     * 
     */
	private Integer deltag;
}