package io.renren.modules.sanswer.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-13
 */
@Data
public class SanswerExcel {
    @Excel(name = "")
    private Integer ansid;
    @Excel(name = "")
    private Integer qid;
    @Excel(name = "用户id")
    private Integer uid;
    @Excel(name = "评论内容")
    private String content;
    @Excel(name = "")
    private Integer dznum;
    @Excel(name = "评论时间")
    private Date time;

}