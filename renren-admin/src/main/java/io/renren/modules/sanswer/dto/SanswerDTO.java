package io.renren.modules.sanswer.dto;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.renren.common.page.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-13
 */
@Data
@ApiModel(value = "")
public class SanswerDTO extends PageInfo implements Serializable  {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")

	@TableId(type = IdType.AUTO)
	private Integer ansid;

	@ApiModelProperty(value = "")
	private Integer qid;

	@ApiModelProperty(value = "用户id")
	private Integer uid;

	@ApiModelProperty(value = "评论内容")
	private String content;

	@ApiModelProperty(value = "")
	private Integer dznum;

	@ApiModelProperty(value = "评论时间")
	private Date time;


}