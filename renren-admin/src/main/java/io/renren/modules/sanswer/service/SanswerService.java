package io.renren.modules.sanswer.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.sanswer.dto.SanswerDTO;
import io.renren.modules.sanswer.entity.SanswerEntity;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-13
 */
public interface SanswerService extends CrudService<SanswerEntity, SanswerDTO> {
    /**
     * 评论信息分页查询
     * @param sanswerDTO
     * @return
     */
    PageData<SanswerDTO> selectByPage(SanswerDTO sanswerDTO);

    /**
     * 保持用户信息
     * @param sanswerDTO
     */
    void saveSanswer(SanswerDTO sanswerDTO);

    /**
     * 修改
     * @param sanswerDTO
     */
    void updateSanswer(SanswerDTO sanswerDTO);

    /**
     * 删除功能
     * @param id
     */
    void deleteSanswer(Integer id);
}