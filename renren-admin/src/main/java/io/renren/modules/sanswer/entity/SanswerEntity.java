package io.renren.modules.sanswer.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-13
 */
@Data
@TableName("sanswer")
public class SanswerEntity {

    /**
     * 
     */
    @TableId
	private Integer ansid;
    /**
     * 
     */
	private Integer qid;
    /**
     * 用户id
     */
	private Integer uid;
    /**
     * 评论内容
     */
	private String content;
    /**
     * 
     */
	private Integer dznum;
    /**
     * 评论时间
     */
	private Date time;
}