package io.renren.modules.sanswer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mchange.lang.IntegerUtils;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.modules.sanswer.dao.SanswerDao;
import io.renren.modules.sanswer.dto.SanswerDTO;
import io.renren.modules.sanswer.entity.SanswerEntity;
import io.renren.modules.sanswer.service.SanswerService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-13
 */
@Service
public class SanswerServiceImpl extends CrudServiceImpl<SanswerDao, SanswerEntity, SanswerDTO> implements SanswerService {

    @Override
    public QueryWrapper<SanswerEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<SanswerEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public PageData<SanswerDTO> selectByPage(SanswerDTO sanswerDTO) {
        IPage<SanswerEntity> page = new Page<>(sanswerDTO.getCurrentPage(), sanswerDTO.getPageSize());
        LambdaQueryWrapper<SanswerEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(sanswerDTO.getContent()),SanswerEntity::getContent,sanswerDTO.getContent());
        IPage<SanswerEntity> result = baseDao.selectPage(page, queryWrapper);

        List<SanswerDTO> list = result.getRecords().stream().map(entity -> {
            SanswerDTO dto = new SanswerDTO();
            BeanUtils.copyProperties(entity, dto);
            return dto;
        }).collect(Collectors.toList());
        /**
         * 必会原始方法
         */
//        ArrayList<SanswerDTO> list = new ArrayList<>();
//
//        for (SanswerEntity record : result.getRecords()) {
//            SanswerDTO dto = new SanswerDTO();
//            BeanUtils.copyProperties(record,dto);
//            list.add(dto);
//        }

        PageData<SanswerDTO> pageData = new PageData<>(list, result.getTotal());
        return pageData;
    }

    @Override
    public void saveSanswer(SanswerDTO sanswerDTO) {
        SanswerEntity sanswerEntity = ConvertUtils.sourceToTarget(sanswerDTO, SanswerEntity.class);
        sanswerEntity.setTime(new Date());
        baseDao.insert(sanswerEntity);
    }

    @Override
    public void updateSanswer(SanswerDTO sanswerDTO) {
        SanswerEntity sanswerEntity = ConvertUtils.sourceToTarget(sanswerDTO, SanswerEntity.class);
        baseDao.updateById(sanswerEntity);
    }

    @Override
    public void deleteSanswer(Integer id) {
        baseDao.deleteById(id);
    }
}