package io.renren.modules.sanswer.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.page.PageInfo;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.sanswer.dto.SanswerDTO;
import io.renren.modules.sanswer.excel.SanswerExcel;
import io.renren.modules.sanswer.service.SanswerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-03-13
 */
@RestController
@RequestMapping("sanswer")
@Api(tags="")
public class SanswerController {
    @Autowired
    private SanswerService sanswerService;

    @GetMapping("page")
    public Result<PageData<SanswerDTO>> page(SanswerDTO sanswerDTO){
        PageData<SanswerDTO> pageData = sanswerService.selectByPage(sanswerDTO);
        return new Result<PageData<SanswerDTO>>().ok(pageData);

    }


//    @GetMapping("page")
//    @ApiOperation("分页")
//    @ApiImplicitParams({
//        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
//        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
//        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
//        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
//    })
//    @RequiresPermissions("sanswer:sanswer:page")
//    public Result<PageData<SanswerDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
//        PageData<SanswerDTO> page = sanswerService.page(params);
//
//        return new Result<PageData<SanswerDTO>>().ok(page);
//    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("sanswer:sanswer:info")
    public Result<SanswerDTO> get(@PathVariable("id") Long id){
        SanswerDTO data = sanswerService.get(id);

        return new Result<SanswerDTO>().ok(data);
    }

//    @PostMapping
//    @ApiOperation("保存")
//    @LogOperation("保存")
//    @RequiresPermissions("sanswer:sanswer:save")
//    public Result save(@RequestBody SanswerDTO dto){
//        //效验数据
//        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);
//
//        sanswerService.save(dto);
//
//        return new Result();
//
//
//    }

    /**
     * 保存
     * @param sanswerDTO
     * @return
     */
    @PostMapping

    public Result saveSanswer(@RequestBody SanswerDTO sanswerDTO){
        sanswerService.saveSanswer(sanswerDTO);
        return new Result();
    }

    /**
     * 修改
     * @param
     * @return
     */
    @PutMapping
    public Result updateSanswer(@RequestBody SanswerDTO sanswerDTO){
        sanswerService.updateSanswer(sanswerDTO);

        return new Result();

    }
//    @PutMapping
//    @ApiOperation("修改")
//    @LogOperation("修改")
//    @RequiresPermissions("sanswer:sanswer:update")
//    public Result update(@RequestBody SanswerDTO dto){
//        //效验数据
//        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);
//
//        sanswerService.update(dto);
//
//        return new Result();
//    }

//

    /**
     * 删除功能
     * @param id
     * @return
     */
    @DeleteMapping("{id}")
    public Result deleteSanswer(@PathVariable Integer id){
        sanswerService.deleteSanswer(id);
        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("sanswer:sanswer:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<SanswerDTO> list = sanswerService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, SanswerExcel.class);
    }

}