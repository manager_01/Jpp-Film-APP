package io.renren.modules.fastdfs.controller;

import com.github.tobato.fastdfs.domain.fdfs.StorePath;
import com.github.tobato.fastdfs.domain.proto.storage.DownloadByteArray;
import com.github.tobato.fastdfs.service.FastFileStorageClient;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("fastDFS")
@Slf4j
public class FastDFSController {

    @Autowired
    private FastFileStorageClient fastFileStorageClient;


    @PostMapping("upload")
    @SneakyThrows
    public Map<String, Object> upload(MultipartFile file) {
        String originalFilename = file.getOriginalFilename();
        String suffix = originalFilename.substring(originalFilename.lastIndexOf(".") + 1);
        StorePath storePath = fastFileStorageClient.uploadFile(file.getInputStream(), file.getSize(), suffix, null);
        String fullPath = storePath.getFullPath();
        Map<String, Object> map = new HashMap<>();
        map.put("status", "ok");
        map.put("code", 200);
        map.put("fullPath", fullPath);
        return map;
    }

    @GetMapping("/download")
    @SneakyThrows
    public void download(String fullPath, HttpServletResponse response) {
        int index = fullPath.indexOf("/");
        String groupName = fullPath.substring(0, index);
        String path = fullPath.substring(index + 1);
        byte[] bytes = fastFileStorageClient.downloadFile(groupName, path, new DownloadByteArray());
        response.setCharacterEncoding("UTF-8");
        response.setHeader("Content-disposition", "attachment;filename=" + URLEncoder.encode("wfxlc.mp3", "UTF-8"));
        ServletOutputStream outputStream = response.getOutputStream();
        IOUtils.write(bytes, outputStream);
    }

    @DeleteMapping("delete")
    public Map<String, Object> delete(String fullPath) {

        fastFileStorageClient.deleteFile(fullPath);

        Map<String, Object> map = new HashMap<>();
        map.put("status", "ok");
        map.put("code", 200);
        map.put("message", "删除成功");
        return map;
    }

}
