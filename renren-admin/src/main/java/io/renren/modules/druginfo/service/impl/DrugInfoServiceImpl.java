package io.renren.modules.druginfo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.renren.common.page.PageData;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.common.utils.ConvertUtils;
import io.renren.modules.druginfo.dao.DrugInfoDao;
import io.renren.modules.druginfo.dto.DrugInfoDTO;
import io.renren.modules.druginfo.dto.DrugInfoQueryDTO;
import io.renren.modules.druginfo.entity.DrugInfoEntity;
import io.renren.modules.druginfo.service.DrugInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author Admin admin@gmail.com
 * @since 1.0.0 2022-12-30
 */
@Service
public class DrugInfoServiceImpl extends CrudServiceImpl<DrugInfoDao, DrugInfoEntity, DrugInfoDTO> implements DrugInfoService {

    @Override
    public QueryWrapper<DrugInfoEntity> getWrapper(Map<String, Object> params) {
        String id = (String) params.get("id");

        QueryWrapper<DrugInfoEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.isNotBlank(id), "id", id);

        return wrapper;
    }


    @Override
    public PageData<DrugInfoDTO> getDrugInfoList(DrugInfoQueryDTO drugInfoQueryDTO) {
        // 构造分页参数
        IPage<DrugInfoEntity> page = new Page<>(drugInfoQueryDTO.getCurrentPage(), drugInfoQueryDTO.getPageSize());

        // 条件构造器
        LambdaQueryWrapper<DrugInfoEntity> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(StringUtils.isNotEmpty(drugInfoQueryDTO.getDrugName()), DrugInfoEntity::getDrugName, drugInfoQueryDTO.getDrugName());

        IPage<DrugInfoEntity> result = baseDao.selectPage(page, queryWrapper);

        // 封装分页数据
        List<DrugInfoDTO> drugInfoDTOList = ConvertUtils.sourceToTarget(result.getRecords(), DrugInfoDTO.class);
        PageData<DrugInfoDTO> pageData = new PageData<>(drugInfoDTOList, result.getTotal());

        return pageData;
    }
}