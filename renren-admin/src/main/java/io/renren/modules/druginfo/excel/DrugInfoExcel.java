package io.renren.modules.druginfo.excel;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 *
 * @author Admin admin@gmail.com
 * @since 1.0.0 2022-12-30
 */
@Data
public class DrugInfoExcel {
    @Excel(name = "")
    private Long id;
    @Excel(name = "品药编码（国家药品编码本位码共14位“86”，代表在我国境内生产、销售的所有药品；国家药品编码本位码类别码为“9”，代表药品；国家药品编码本位码本体码的前5位为药品企业标识）")
    private String drugCode;
    @Excel(name = "药品名称")
    private String drugName;
    @Excel(name = "品药规格")
    private String drugFormat;
    @Excel(name = "包装单位")
    private String drugUnit;
    @Excel(name = "生产厂家")
    private String manufacturer;
    @Excel(name = "药剂类型")
    private String drugDosage;
    @Excel(name = "药品类型")
    private String drugType;
    @Excel(name = "药品单价")
    private BigDecimal drugPrice;
    @Excel(name = "拼音助记码")
    private String mnemonicCode;
    @Excel(name = "创建时间")
    private Date creationDate;

}