package io.renren.modules.druginfo.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

import java.math.BigDecimal;

/**
 * 
 *
 * @author Admin admin@gmail.com
 * @since 1.0.0 2022-12-30
 */
@Data
@ApiModel(value = "")
public class DrugInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "")
	private Long id;

	@ApiModelProperty(value = "品药编码（国家药品编码本位码共14位“86”，代表在我国境内生产、销售的所有药品；国家药品编码本位码类别码为“9”，代表药品；国家药品编码本位码本体码的前5位为药品企业标识）")
	private String drugCode;

	@ApiModelProperty(value = "药品名称")
	private String drugName;

	@ApiModelProperty(value = "品药规格")
	private String drugFormat;

	@ApiModelProperty(value = "包装单位")
	private String drugUnit;

	@ApiModelProperty(value = "生产厂家")
	private String manufacturer;

	@ApiModelProperty(value = "药剂类型")
	private String drugDosage;

	@ApiModelProperty(value = "药品类型")
	private String drugType;

	@ApiModelProperty(value = "药品单价")
	private BigDecimal drugPrice;

	@ApiModelProperty(value = "拼音助记码")
	private String mnemonicCode;

	@ApiModelProperty(value = "创建时间")
	private Date creationDate;


}