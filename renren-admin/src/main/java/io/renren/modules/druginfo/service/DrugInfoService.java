package io.renren.modules.druginfo.service;

import io.renren.common.page.PageData;
import io.renren.common.service.CrudService;
import io.renren.modules.druginfo.dto.DrugInfoDTO;
import io.renren.modules.druginfo.dto.DrugInfoQueryDTO;
import io.renren.modules.druginfo.entity.DrugInfoEntity;

/**
 * 
 *
 * @author Admin admin@gmail.com
 * @since 1.0.0 2022-12-30
 */
public interface DrugInfoService extends CrudService<DrugInfoEntity, DrugInfoDTO> {

    /**
     * 分页查询药品信息
     * @param drugInfoQueryDTO
     * @return
     */
    PageData<DrugInfoDTO> getDrugInfoList(DrugInfoQueryDTO drugInfoQueryDTO);
}