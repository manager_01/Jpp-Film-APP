package io.renren.modules.druginfo.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.druginfo.entity.DrugInfoEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 *
 * @author Admin admin@gmail.com
 * @since 1.0.0 2022-12-30
 */
@Mapper
public interface DrugInfoDao extends BaseDao<DrugInfoEntity> {
	
}