package io.renren.modules.druginfo.controller;

import io.renren.common.annotation.LogOperation;
import io.renren.common.constant.Constant;
import io.renren.common.page.PageData;
import io.renren.common.utils.ExcelUtils;
import io.renren.common.utils.Result;
import io.renren.common.validator.AssertUtils;
import io.renren.common.validator.ValidatorUtils;
import io.renren.common.validator.group.AddGroup;
import io.renren.common.validator.group.DefaultGroup;
import io.renren.common.validator.group.UpdateGroup;
import io.renren.modules.druginfo.dto.DrugInfoDTO;
import io.renren.modules.druginfo.dto.DrugInfoQueryDTO;
import io.renren.modules.druginfo.excel.DrugInfoExcel;
import io.renren.modules.druginfo.service.DrugInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


/**
 * 
 *
 * @author Admin admin@gmail.com
 * @since 1.0.0 2022-12-30
 */
@RestController
@RequestMapping("druginfo")
@Api(tags="")
public class DrugInfoController {
    @Autowired
    private DrugInfoService drugInfoService;

    /**
     * 分页查询药品信息
     * @param drugInfoQueryDTO
     * @return
     */
    @GetMapping
    public Result<PageData<DrugInfoDTO>> getDrugInfoList(DrugInfoQueryDTO drugInfoQueryDTO){
        PageData<DrugInfoDTO> page = drugInfoService.getDrugInfoList(drugInfoQueryDTO);
        return new Result<PageData<DrugInfoDTO>>().ok(page);
    }

    @GetMapping("page")
    @ApiOperation("分页")
    @ApiImplicitParams({
        @ApiImplicitParam(name = Constant.PAGE, value = "当前页码，从1开始", paramType = "query", required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.LIMIT, value = "每页显示记录数", paramType = "query",required = true, dataType="int") ,
        @ApiImplicitParam(name = Constant.ORDER_FIELD, value = "排序字段", paramType = "query", dataType="String") ,
        @ApiImplicitParam(name = Constant.ORDER, value = "排序方式，可选值(asc、desc)", paramType = "query", dataType="String")
    })
    @RequiresPermissions("druginfo:druginfo:page")
    public Result<PageData<DrugInfoDTO>> page(@ApiIgnore @RequestParam Map<String, Object> params){
        PageData<DrugInfoDTO> page = drugInfoService.page(params);

        return new Result<PageData<DrugInfoDTO>>().ok(page);
    }

    @GetMapping("{id}")
    @ApiOperation("信息")
    @RequiresPermissions("druginfo:druginfo:info")
    public Result<DrugInfoDTO> get(@PathVariable("id") Long id){
        DrugInfoDTO data = drugInfoService.get(id);

        return new Result<DrugInfoDTO>().ok(data);
    }

    @PostMapping
    @ApiOperation("保存")
    @LogOperation("保存")
    @RequiresPermissions("druginfo:druginfo:save")
    public Result save(@RequestBody DrugInfoDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, AddGroup.class, DefaultGroup.class);

        drugInfoService.save(dto);

        return new Result();
    }

    @PutMapping
    @ApiOperation("修改")
    @LogOperation("修改")
    @RequiresPermissions("druginfo:druginfo:update")
    public Result update(@RequestBody DrugInfoDTO dto){
        //效验数据
        ValidatorUtils.validateEntity(dto, UpdateGroup.class, DefaultGroup.class);

        drugInfoService.update(dto);

        return new Result();
    }

    @DeleteMapping
    @ApiOperation("删除")
    @LogOperation("删除")
    @RequiresPermissions("druginfo:druginfo:delete")
    public Result delete(@RequestBody Long[] ids){
        //效验数据
        AssertUtils.isArrayEmpty(ids, "id");

        drugInfoService.delete(ids);

        return new Result();
    }

    @GetMapping("export")
    @ApiOperation("导出")
    @LogOperation("导出")
    @RequiresPermissions("druginfo:druginfo:export")
    public void export(@ApiIgnore @RequestParam Map<String, Object> params, HttpServletResponse response) throws Exception {
        List<DrugInfoDTO> list = drugInfoService.list(params);

        ExcelUtils.exportExcelToTarget(response, null, list, DrugInfoExcel.class);
    }

}