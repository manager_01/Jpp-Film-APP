package io.renren.modules.druginfo.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 
 *
 * @author Admin admin@gmail.com
 * @since 1.0.0 2022-12-30
 */
@Data
@TableName("drug_info")
public class DrugInfoEntity {

    /**
     * 
     */
	private Long id;
    /**
     * 品药编码（国家药品编码本位码共14位“86”，代表在我国境内生产、销售的所有药品；国家药品编码本位码类别码为“9”，代表药品；国家药品编码本位码本体码的前5位为药品企业标识）
     */
	private String drugCode;
    /**
     * 药品名称
     */
	private String drugName;
    /**
     * 品药规格
     */
	private String drugFormat;
    /**
     * 包装单位
     */
	private String drugUnit;
    /**
     * 生产厂家
     */
	private String manufacturer;
    /**
     * 药剂类型
     */
	private String drugDosage;
    /**
     * 药品类型
     */
	private String drugType;
    /**
     * 药品单价
     */
	private BigDecimal drugPrice;
    /**
     * 拼音助记码
     */
	private String mnemonicCode;
    /**
     * 创建时间
     */
	private Date creationDate;
}