package io.renren.modules.axiostest.dto;

import lombok.Data;

@Data
public class Dept {
    private String dName;
}
