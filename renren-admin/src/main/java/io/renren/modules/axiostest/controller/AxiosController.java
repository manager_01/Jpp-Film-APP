package io.renren.modules.axiostest.controller;

import io.renren.common.utils.Result;
import io.renren.modules.axiostest.dto.UserDTO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("test")
@Slf4j
public class AxiosController {

    @GetMapping("user")
    public Result<String> testGet(UserDTO userDTO) {
        log.info("Get - {}", userDTO);
        return new Result<String>().ok("success");
    }

    @GetMapping("user/{id}")
    public Result<String> testGetRestful(@PathVariable String id) {
        log.info("GetRestful - {}", id);
        return new Result<String>().ok("success");
    }

    @PostMapping("user")
    public Result<String> testPost(@RequestBody UserDTO userDTO) {
        log.info("Post - {}", userDTO);
        return new Result<String>().ok("success");
    }


    @PutMapping("user")
    public Result<String> testPut(@RequestBody UserDTO userDTO) {
        log.info("Put - {}", userDTO);
        return new Result<String>().ok("success");
    }

    @DeleteMapping("user")
    public Result<String> testDelete(@RequestBody UserDTO userDTO) {
        log.info("Delete - {}", userDTO);
        return new Result<String>().ok("success");
    }

    @DeleteMapping("user/{id}")
    public Result<String> testDeleteRestful(@PathVariable String id) {
        log.info("DeleteRestful - {}", id);
        return new Result<String>().ok("success");
    }
}
