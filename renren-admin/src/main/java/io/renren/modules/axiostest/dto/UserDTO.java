package io.renren.modules.axiostest.dto;

import io.swagger.annotations.ApiModel;
import lombok.Data;

@Data
@ApiModel(value = "用户信息，用于测试axios请求")
public class UserDTO {

    private static final long serialVersionUID = 1L;

    private String username;
    private int age;

    private Dept dept;
}
