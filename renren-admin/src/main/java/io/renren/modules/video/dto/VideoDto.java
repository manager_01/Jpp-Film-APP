package io.renren.modules.video.dto;

import lombok.Data;

@Data
public class VideoDto {

    private String url;
}
