package io.renren.modules.video.service.impl;

import com.alibaba.fastjson.JSON;
import io.renren.modules.video.dto.VideoDto;
import io.renren.modules.video.service.VideoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class VideoServiceImpl implements VideoService {

    public static final String VIDEO_KEY = "video";


    @Autowired
    private StringRedisTemplate redisTemplate;

    @Override
    public VideoDto getOneVideo() {

        VideoDto videoDto;

        String videoStr = redisTemplate.opsForValue().get(VIDEO_KEY);

        if (StringUtils.isNotEmpty(videoStr)) {
            videoDto = JSON.parseObject(videoStr, VideoDto.class);
        } else {
            videoDto = getVideoFromDatabase();
            redisTemplate.opsForValue().set(VIDEO_KEY, JSON.toJSONString(videoDto));
        }

        return videoDto;
    }


    private VideoDto getVideoFromDatabase() {
        VideoDto videoDto = new VideoDto();
        videoDto.setUrl("http://10.25.34.157:8888/group1/M00/00/00/ChkinWQOg1SAINeKAPi2oL7KWR4398.MP4");
        return videoDto;
    }
}
