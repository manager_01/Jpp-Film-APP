package io.renren.modules.video.controller;

import io.renren.common.utils.Result;
import io.renren.modules.video.dto.VideoDto;
import io.renren.modules.video.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("video")
public class VideoController {

    @Autowired
    private VideoService videoService;

    @GetMapping("one")
    public Result getOneVideo(){
       VideoDto videoDto =  videoService.getOneVideo();
       return new Result().ok(videoDto);
    }
}
