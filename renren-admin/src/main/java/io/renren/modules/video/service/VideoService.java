package io.renren.modules.video.service;

import io.renren.modules.video.dto.VideoDto;

public interface VideoService {
    VideoDto getOneVideo();
}
